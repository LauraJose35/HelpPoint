﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarTutores : ContentPage
    {
        PersonaCuidada personaCuidad;
        IManagerPersonaCuidada manejadorPersona;
        IManagerTutor manajerTutor;
        HelpPoint.COMMON.Entidades.Tutores model;
        public AgregarTutores(PersonaCuidada persona)
        {
            InitializeComponent();
            manejadorPersona = Tools.FabricManager.PersonaCuidadaManager();
            manajerTutor = Tools.FabricManager.TutorManager();
            personaCuidad = persona;
            model = BindingContext as HelpPoint.COMMON.Entidades.Tutores;
        }

        private void btnAgregar_Clicked(object sender, EventArgs e)
        {
            HelpPoint.COMMON.Entidades.Tutores tutores = BindingContext as HelpPoint.COMMON.Entidades.Tutores;
            if (manajerTutor.Leer.Where(p => p.Nombre == model.Nombre && p.ApPaterno == model.ApPaterno).SingleOrDefault() == null)
            {
                model.PersonaCuidada = personaCuidad;
                if (manajerTutor.Insertar(tutores))
                {
                    DisplayAlert("Tutores", "Tutor registrado con exitoso", "Ok");
                    this.BindingContext = new HelpPoint.COMMON.Entidades.Tutores();

                    Navigation.PushAsync(new Tutores(personaCuidad));
                }
                else
                {
                    DisplayAlert("Error", manajerTutor.Error, "Ok");
                }
            }
        }
    }
}