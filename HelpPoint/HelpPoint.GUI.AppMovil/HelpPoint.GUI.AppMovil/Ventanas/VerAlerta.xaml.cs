﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerAlerta : ContentPage
    {
        Alertas alertas;
        IManagerAlertas managerAlertas;
        public VerAlerta(Alertas Alertas)
        {
            InitializeComponent();
            managerAlertas = Tools.FabricManager.AlertasManager();
            alertas = Alertas;
            LlenarCampos();
        }

        private void LlenarCampos()
        {
            if (managerAlertas.Leer.Where(p => p.Id == alertas.Id).Count()==1)
            {
                txtFecha.Text = alertas.FechaHora.ToString();
                txtUbicacion.Text = alertas.Ubicacion;
                listAlerta.ItemsSource = null;
                listAlerta.ItemsSource = alertas.IdTutor;
            }
            else {
                DisplayAlert("Alertas", "No se puede visualizar esta alerta", "Ok");
            }
        }
    }
}