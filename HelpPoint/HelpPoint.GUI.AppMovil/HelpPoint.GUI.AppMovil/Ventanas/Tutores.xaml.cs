﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tutores : ContentPage
    {
        PersonaCuidada personaCuidada;
        IManagerPersonaCuidada manejadorPersona;
        IManagerTutor manajerTutor;
        COMMON.Entidades.Tutores _Tutores;
        public Tutores(PersonaCuidada persona)
        {
            InitializeComponent();
            manejadorPersona = Tools.FabricManager.PersonaCuidadaManager();
            manajerTutor = Tools.FabricManager.TutorManager();
            personaCuidada = persona;
            _Tutores = new COMMON.Entidades.Tutores();

            ActualizarTabla(personaCuidada);
        }
        private void btnAgregar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgregarTutores(personaCuidada));
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            if (listaTutores.SelectedItem is HelpPoint.COMMON.Entidades.Tutores tutores)
            {
                if (manajerTutor.Eliminar(tutores.Id))
                {
                    DisplayAlert("Tutores", "Se elimino el Tutor: " + tutores.Nombre, "Ok");
                    ActualizarTabla(personaCuidada);
                }
                else
                {
                    if (manajerTutor.Leer.Where(p => p.Id == tutores.Id).SingleOrDefault() != null)
                        DisplayAlert("Error", "Error al eliminar a el Tutor: " + tutores.Nombre, "Ok");
                    else
                        DisplayAlert("Error", "El dato ya no existe", "Ok");
                }
            }
            else {
                DisplayAlert("Tutores", "Seleccione una fila de la tabla", "Ok");
            }
        }

        public void ActualizarTabla(PersonaCuidada datospersona)
        {
            listaTutores.ItemsSource = null;
            listaTutores.ItemsSource = manajerTutor.Leer.Where(p=>p.PersonaCuidada.Id== personaCuidada.Id);
        }

        private void btnActualizar_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (listaTutores.SelectedItem != null)
                {
                    _Tutores = listaTutores.SelectedItem as COMMON.Entidades.Tutores;
                    if (listaTutores.SelectedItem != null)
                    {
                        Navigation.PushAsync(new ActualizarTutor(_Tutores, personaCuidada));
                    }
                    
                }
                else
                {
                    DisplayAlert("Error", "Seleccione una fila de la tabla", "Ok");
                }

            }
            catch (Exception)
            {
                DisplayAlert("Error", "Primero tiene que seleccionar una fila de la tabla", "Ok");
            }
        }

        private void txtNombreTutor_TextChanged(object sender, TextChangedEventArgs e)
        {
            listaTutores.ItemsSource = null;
            if (string.IsNullOrWhiteSpace(txtNombreTutor.Text))
                listaTutores.ItemsSource = manajerTutor.Leer.Where(p => p.PersonaCuidada.Id == personaCuidada.Id);
            else
                listaTutores.ItemsSource = manajerTutor.Leer.Where(p => p.PersonaCuidada.Id == personaCuidada.Id && p.Nombre.ToLower() == txtNombreTutor.Text.ToLower() || p.ApMaterno.ToLower() == txtNombreTutor.Text.ToLower() || p.ApPaterno.ToLower() == txtNombreTutor.Text.ToLower());

        }

        
    }
}