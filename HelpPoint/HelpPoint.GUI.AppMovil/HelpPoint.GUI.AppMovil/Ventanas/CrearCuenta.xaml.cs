﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearCuenta : ContentPage
    {
        IManagerPersonaCuidada managerPersona;
       HelpPoint.COMMON.Entidades.PersonaCuidada modelPersona;
        public CrearCuenta()
        {
            InitializeComponent();
            managerPersona = Tools.FabricManager.PersonaCuidadaManager();
            modelPersona = BindingContext as HelpPoint.COMMON.Entidades.PersonaCuidada;

            //NavigationPage.SetHasNavigationBar(this, false);
        }

        private void btnAgregar_Clicked(object sender, EventArgs e)
        {
            try
            {

                modelPersona = BindingContext as HelpPoint.COMMON.Entidades.PersonaCuidada;
                PersonaCuidada persona = BindingContext as PersonaCuidada;
                persona.Genero = pinGenero.Items[pinGenero.SelectedIndex];
                persona.FechaNacimiento = DataPincker.Date;
                persona.Edad = int.Parse(txtEdad.Text);
                if (managerPersona.Leer.Where(p => p.Correo == persona.Correo).SingleOrDefault() == null) 
                {
                    if (managerPersona.Insertar(persona))
                    {
                        DisplayAlert("Persona Cuidada", "Registro exitoso", "Ok");
                        this.Navigation.PushAsync(new Login());
                    }
                    else
                    {
                        DisplayAlert("Error", managerPersona.Error, "Ok");


                    }
                }
                else {
                    DisplayAlert("Error", "Correo existente, use otro", "Ok");
                }
            }
            catch (Exception)
            {
                DisplayAlert("Error", managerPersona.Error, "Ok");
            }

        }
    }
}