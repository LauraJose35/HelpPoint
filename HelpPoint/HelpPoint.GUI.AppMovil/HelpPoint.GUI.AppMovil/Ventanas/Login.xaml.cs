﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using HelpPoint.GUI.AppMovil.Modelos;
using HelpPoint.GUI.AppMovil.Ventanas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        IManagerPersonaCuidada managerPersona;
        LoginModel model;
        public Login()
        {
            InitializeComponent();
            managerPersona = Tools.FabricManager.PersonaCuidadaManager();
            model = BindingContext as LoginModel;

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void btnCrearCuenta_Clicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new CrearCuenta());
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "Ok");
            }
        }

        private void btnIngresar_Clicked(object sender, EventArgs e)
        {
            try
            {
                model = BindingContext as LoginModel;
                PersonaCuidada personaCuidada = managerPersona.Leer.Where(p => p.Contrasenia == model.Contrasenia&& p.Correo.ToUpper() == model.Correo.ToUpper()).SingleOrDefault();
                try
                {
                    if (personaCuidada != null)
                    {
                        DisplayAlert("Help-Point", "Bienvenido " + personaCuidada.Nombre, "Ok");
                        Navigation.PushAsync(new HelpPoint.GUI.AppMovil.Ventanas.Menu(personaCuidada));
                        this.BindingContext = new LoginModel();
                    }
                    else
                    {
                        DisplayAlert("Error", "Usuario o contraseña incorrecta", "Ok");
                    }
                }
                catch (Exception ex)
                {
                    DisplayAlert("Error", ex.Message, "Ok");
                }
            }
            catch (Exception)
            {
                DisplayAlert("Help-Point", "No ha llenado los campos", "Ok");
            }
           
            
        }
    }
}