﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActualizarTutor : ContentPage
    {
        COMMON.Entidades.Tutores persona;

        IManagerPersonaCuidada manejadorPersona;
        IManagerTutor manajerTutor;
        HelpPoint.COMMON.Entidades.Tutores model;
        COMMON.Entidades.PersonaCuidada _personaCuidada;
        public ActualizarTutor(COMMON.Entidades.Tutores _persona, COMMON.Entidades.PersonaCuidada personaCuidada )
        {
            InitializeComponent();
            manejadorPersona = Tools.FabricManager.PersonaCuidadaManager();
            manajerTutor = Tools.FabricManager.TutorManager();
            //modelPersona = BindingContext as HelpPoint.COMMON.Entidades.PersonaCuidada;
            persona = _persona;
            _personaCuidada = personaCuidada;
            LlenarCampos();

        }

        private void LlenarCampos()
        {
            if (manajerTutor.Leer.Where(p => p.Id == persona.Id).Count() == 1)
            {
                txtNombre.Text = persona.Nombre;
                txtApPaterno.Text = persona.ApPaterno;
                txtApMaterno.Text = persona.ApMaterno;
                txtDireccion.Text = persona.Direccion;
                txtOcupacion.Text = persona.Ocupacion;
                txtNumeroTelefono.Text = persona.NumeroTelefono;
            }
            else {
                Navigation.PushAsync( new Tutores(_personaCuidada));
            }
        }

        private async void btnActualizar_Clicked(object sender, EventArgs e)
        {
            try
            {
                model = BindingContext as COMMON.Entidades.Tutores;
                COMMON.Entidades.Tutores p = model as COMMON.Entidades.Tutores;
                p.Id = persona.Id;
                p.PersonaCuidada = _personaCuidada;

                if (manajerTutor.Actualizar(p))
                {
                    await DisplayAlert("Tutor", "Actualización correcta", "Ok");
                    persona = p;
                    LlenarCampos();
                    await Navigation.PushAsync(new Tutores(_personaCuidada));
                }
                else
                {
                    var result = await DisplayAlert("Tutor", "El tutor no ha sido editado", "Ok", "Cancel");
                    if (result == true)
                        await Navigation.PushAsync(new Tutores(_personaCuidada));
                }
            }
            catch (Exception)
            {

                await DisplayAlert("Error", "Error al actualizar al tutor", "Ok");
            }
        }
    }
}