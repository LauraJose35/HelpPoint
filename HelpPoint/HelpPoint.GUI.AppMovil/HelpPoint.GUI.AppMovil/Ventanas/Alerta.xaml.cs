﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Alerta : ContentPage
    {
        IManagerAlertas managerAlertas;
        IManagerTutor managerTutor;
        HelpPoint.COMMON.Entidades.PersonaCuidada persona;
        Alertas _Alertas;
        
        public Alerta(HelpPoint.COMMON.Entidades.PersonaCuidada Persona)
        {
            InitializeComponent();
            managerAlertas = Tools.FabricManager.AlertasManager();
            managerTutor = Tools.FabricManager.TutorManager();
            _Alertas = new Alertas();
            persona = Persona;
            ActualizarTabla();
        }

        private List<COMMON.Entidades.Tutores> InsertarDatos()
        {
            List<COMMON.Entidades.Tutores> datos = new List<COMMON.Entidades.Tutores>();
            foreach (var item in managerTutor.Leer)
            {
                if (item.PersonaCuidada.Id == persona.Id) {
                    COMMON.Entidades.Tutores tutores = new COMMON.Entidades.Tutores();
                    tutores = item;
                    datos.Add(tutores);
                }
            }
            return datos;
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            if (listAlerta.SelectedItem is Alertas alertas)
            {
                if (managerAlertas.Eliminar(alertas.Id))
                {
                    DisplayAlert("Eliminación", "Se ha eliminado correctamente la alerta", "Ok");
                    ActualizarTabla();
                }
                else
                {
                    DisplayAlert("Error", "Error al eliminar la alerta", "Ok");
                }
            }
        }

        private void ActualizarTabla()
        {
            listAlerta.ItemsSource = null;
            listAlerta.ItemsSource = managerAlertas.Leer.Where(p=>p.IdPersonas.Id==persona.Id).OrderBy(p=>p.FechaHora);
        }

        private void listAlerta_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (listAlerta.SelectedItem != null)
            {
                _Alertas = listAlerta.SelectedItem as Alertas;
                
            }
            else {
                DisplayAlert("Error", "Seleccione una fila de la tabla", "Ok");
            }
        }

        private void btnVer_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (listAlerta.SelectedItem != null)
                {
                    _Alertas = listAlerta.SelectedItem as Alertas;
                    if (listAlerta.SelectedItem != null)
                    {
                        Navigation.PushAsync(new VerAlerta(_Alertas));
                    }
                }
                else
                {
                    DisplayAlert("Error", "Seleccione una fila de la tabla", "Ok");
                }
                
            }
            catch (Exception)
            {
                DisplayAlert("Error", "Primero tiene que seleccionar una fila de la tabla", "Ok");
            }
           
        }
    }
}