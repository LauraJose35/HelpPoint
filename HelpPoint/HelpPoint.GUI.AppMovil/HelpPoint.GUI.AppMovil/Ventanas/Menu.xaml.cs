﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : MasterDetailPage
    {
        HelpPoint.COMMON.Entidades.PersonaCuidada persona;
        public Menu(HelpPoint.COMMON.Entidades.PersonaCuidada Persona)
        {
            InitializeComponent();
            persona = Persona;
            MyMenu();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        public void MyMenu()
        {
            Detail = new NavigationPage(new Feed());
            List<MenuClass> menu = new List<MenuClass>
            {
                new MenuClass{ Page= new Tutores(persona),Titulo="Tutores", icon="user.png"},
                new MenuClass{ Page= new ActualizarCuidado(persona),Titulo="Cuidado", icon="cuidado.png"},
                new MenuClass{ Page= new Alerta(persona),Titulo="Alertas", icon="reporte.png"},
                new MenuClass{ Page= new Login(),Titulo="Salir", icon="cerrar.png" }
            };
            ListMenu.ItemsSource = menu;
        }


        private void ListMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var menu = e.SelectedItem as MenuClass;
            if (menu != null)
            {
                IsPresented = false;
                Detail = new NavigationPage(menu.Page);
            }
        }
        public class MenuClass
        {
            public string Titulo
            {
                get;
                set;
            }

            public ImageSource icon
            {
                get;
                set;
            }

            public Page Page
            {
                get;
                set;
            }
        }
    }
}