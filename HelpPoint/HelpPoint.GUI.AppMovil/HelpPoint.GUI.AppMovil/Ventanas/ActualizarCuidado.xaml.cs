﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActualizarCuidado : ContentPage
    {
        IManagerPersonaCuidada managerPersona;
        HelpPoint.COMMON.Entidades.PersonaCuidada modelPersona;
        HelpPoint.COMMON.Entidades.PersonaCuidada personaCuidada;
        public ActualizarCuidado(HelpPoint.COMMON.Entidades.PersonaCuidada PersonaCuidada)
        {
            InitializeComponent();
            managerPersona = Tools.FabricManager.PersonaCuidadaManager();
           // modelPersona = BindingContext as HelpPoint.COMMON.Entidades.PersonaCuidada;
            personaCuidada = PersonaCuidada;
            LlenarCampos();
        }

        private void LlenarCampos()
        {
            txtNombre.Text = personaCuidada.Nombre;
            txtApPaterno.Text = personaCuidada.ApPaterno;
            txtApMaterno.Text = personaCuidada.ApMaterno;
            txtContrasenia.Text = personaCuidada.Contrasenia;
            txtCorreo.Text = personaCuidada.Correo;
            txtDireccion.Text = personaCuidada.Direccion;
            txtEdad.Text = personaCuidada.Edad.ToString();
            txtEstatura.Text = personaCuidada.Estatura;
            txtIdDispositivo.Text = personaCuidada.IdDispositivo;
            txtocupacion.Text = personaCuidada.Ocupacion;
            txtNumeroTelefono.Text = personaCuidada.NumeroTelefono;
            pinGenero.SelectedItem = personaCuidada.Genero;
            dataFechaNacimiento.Date = personaCuidada.FechaNacimiento;
        }

        private  void btnActualizar_Clicked(object sender, EventArgs e)
        {
            try
            {
                modelPersona = BindingContext as PersonaCuidada;
                PersonaCuidada persona = modelPersona as PersonaCuidada;
                persona.Genero = pinGenero.Items[pinGenero.SelectedIndex];
                persona.Id = personaCuidada.Id;
                persona.FechaNacimiento = dataFechaNacimiento.Date;
                if (managerPersona.Actualizar(persona))
                {
                     DisplayAlert("Persona cuidada", "Actualización correcta", "Ok");
                    personaCuidada = persona;
                    LlenarCampos();
                }
                else
                {
                     DisplayAlert("Tutor", "Sus datos no ha sido editado", "Ok");
                }
            }
            catch (Exception )
            {

                DisplayAlert("Error", "Error al actualizar a la persona cuidada", "Ok");
            }
        }
    }
}