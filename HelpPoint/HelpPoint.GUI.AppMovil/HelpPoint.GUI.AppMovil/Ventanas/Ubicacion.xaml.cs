﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelpPoint.GUI.AppMovil.Ventanas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Ubicacion : ContentPage
    {
        IManagerUbicaciones managerUbicacionees;
        HelpPoint.COMMON.Entidades.PersonaCuidada persona;
        public Ubicacion(HelpPoint.COMMON.Entidades.PersonaCuidada Persona)
        {
            InitializeComponent();
            managerUbicacionees = Tools.FabricManager.UbicacionesManager();
            persona = Persona;
            ActualizarTabla();
        }

        private void ActualizarTabla()
        {
            listUbicaciones.ItemsSource = null;
            listUbicaciones.ItemsSource = managerUbicacionees.Leer.Where(p => p.IdPersona == persona.Id.ToString());
        }

        private void btnBuscar_Clicked(object sender, EventArgs e)
        {
            listUbicaciones.ItemsSource = null;
            listUbicaciones.ItemsSource = managerUbicacionees.Leer.Where(p => p.IdPersona == persona.Id.ToString() && FechaAlerta.TabIndex.ToString() == p.FechaHora.ToString());
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {

            if (listUbicaciones.SelectedItem is Ubicaciones ubicaciones)
            {
                if (managerUbicacionees.Eliminar(ubicaciones.Id))
                {
                    DisplayAlert("Eliminación", "Se ha eliminado correctamente la ubicación", "Ok");
                    ActualizarTabla();
                }
                else
                {
                    DisplayAlert("Error", "Error al eliminar la ubicación", "Ok");
                }
            }
        }

        private void listUbicaciones_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (listUbicaciones.SelectedItem != null)
            {
                Ubicaciones ubicaciones = listUbicaciones.SelectedItem as Ubicaciones;
            }
        }
    }
}