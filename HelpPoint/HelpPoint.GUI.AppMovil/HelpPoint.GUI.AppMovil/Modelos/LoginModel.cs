﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.GUI.AppMovil.Modelos
{
    public class LoginModel
    {
        public string Correo { get; set; }
        public string Contrasenia { get; set; }
    }
}
