﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Entidades
{
    public class Tutores:Persona
    {
        public PersonaCuidada PersonaCuidada { get; set; }
        public override string ToString()
        {
            return Nombre + " " + ApPaterno + " " +ApMaterno + " Núm Tel: " + NumeroTelefono;            
        }
    }
}
