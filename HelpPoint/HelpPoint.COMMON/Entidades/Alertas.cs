﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Entidades
{
    public class Alertas:BaseDTO
    {
        public DateTime FechaHora { get; set; }
        public string Ubicacion { get; set; }
        public PersonaCuidada IdPersonas { get; set; }
        public List<Tutores> IdTutor { get; set; }
        public override string ToString()
        {
            return "Fecha= " + FechaHora.Date + " Ubicación" + Ubicacion;
        }
    }
}
