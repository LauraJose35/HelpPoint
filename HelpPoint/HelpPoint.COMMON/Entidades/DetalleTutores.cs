﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Entidades
{
    public class DetalleTutores:BaseDTO
    {
        public Tutores IdTutores { get; set; }
        public PersonaCuidada IdPersonaCuidada { get; set; }
    }
}
