﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Entidades
{
    public class Persona:BaseDTO
    {
        public string Nombre { get; set; }
        public string ApPaterno { get; set; }
        public string ApMaterno { get; set; }
        public string Direccion { get; set; }
        public string NumeroTelefono { get; set; }
        public string Ocupacion { get; set; }
    }
}
