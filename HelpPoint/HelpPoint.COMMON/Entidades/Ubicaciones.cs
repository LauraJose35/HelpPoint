﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Entidades
{
    public class Ubicaciones:BaseDTO
    {
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public DateTime FechaHora { get; set; }
        public string IdPersona { get; set; }
    }
}
