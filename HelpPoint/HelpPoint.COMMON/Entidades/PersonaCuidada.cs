﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Entidades
{
    public class PersonaCuidada:Persona
    {
        public int Edad { get; set; }
        public string Estatura { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public string Contrasenia { get; set; }
        public string Correo { get; set; }
        public string IdDispositivo { get; set; }
    }
}
