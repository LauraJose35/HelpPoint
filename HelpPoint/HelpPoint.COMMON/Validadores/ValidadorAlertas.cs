﻿using FluentValidation;
using HelpPoint.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Validadores
{
    public class ValidadorAlertas : AbstractValidator<Alertas>
    {
        public ValidadorAlertas()
        {
            RuleFor(e => e.FechaHora).NotNull().NotEmpty();
            RuleFor(e=>e.IdPersonas).NotNull().NotEmpty();
            RuleFor(e=>e.Ubicacion).NotNull().NotEmpty();
        }
    }
}