﻿using FluentValidation;
using HelpPoint.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Validadores
{
    public class ValidadorTutor:AbstractValidator<Tutores>
    {
        public ValidadorTutor()
        {
            RuleFor(e => e.ApMaterno).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.ApPaterno).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.Direccion).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.Nombre).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.NumeroTelefono).NotEmpty().NotNull().MaximumLength(14);
            RuleFor(e => e.Ocupacion).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.PersonaCuidada).NotEmpty().NotNull();
        }
    }
}
