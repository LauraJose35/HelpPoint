﻿using FluentValidation;
using HelpPoint.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Validadores
{
    public class ValidadorDetalleTutores:AbstractValidator<DetalleTutores>
    {
        public ValidadorDetalleTutores()
        {
            RuleFor(e => e.IdPersonaCuidada).NotNull().NotEmpty();
            RuleFor(e => e.IdTutores).NotNull().NotEmpty();
        }
    }
}
