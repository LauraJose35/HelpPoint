﻿using FluentValidation;
using HelpPoint.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Validadores
{
    public class ValidadorUbicaciones:AbstractValidator<Ubicaciones>
    {
        public ValidadorUbicaciones()
        {
            RuleFor(e => e.FechaHora).NotNull().NotEmpty();
            RuleFor(e => e.Latitud).NotNull().NotEmpty();
            RuleFor(e => e.Longitud).NotNull().NotEmpty();
        }
    }
}
