﻿using FluentValidation;
using HelpPoint.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Validadores
{
    public class ValidadorPersonaCuidada:AbstractValidator<PersonaCuidada>
    {
        public ValidadorPersonaCuidada()
        {
            RuleFor(e=>e.ApMaterno).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.ApPaterno).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.Direccion).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.Edad).NotEmpty().NotNull();
            RuleFor(e => e.Estatura).NotEmpty().NotNull();
            RuleFor(e => e.Nombre).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.NumeroTelefono).NotEmpty().NotNull().MaximumLength(14);
            RuleFor(e => e.Ocupacion).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.Correo).Length(1, 50).NotEmpty().NotNull().EmailAddress();
            RuleFor(e => e.Contrasenia).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.FechaNacimiento).NotEmpty().NotNull();
            RuleFor(e => e.Genero).Length(1, 50).NotEmpty().NotNull();
            RuleFor(e => e.IdDispositivo).Length(1, 16).NotEmpty().NotNull();
        }
    }
}
