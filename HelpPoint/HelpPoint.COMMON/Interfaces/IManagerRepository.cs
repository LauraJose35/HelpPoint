﻿using HelpPoint.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Interfaces
{
    public interface IManagerRepository<T> where T:BaseDTO
    {
        string Error { get; }
        IEnumerable<T> Leer { get; }
        bool Eliminar(ObjectId Id);
        bool Insertar(T Entidad);
        bool Actualizar(T Entidad);
    }
}
