﻿using HelpPoint.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.COMMON.Interfaces
{
    public interface IManagerGeneric<T> where T:BaseDTO 
    {
        string Error { get; }
        IEnumerable<T> Leer { get; }
        bool Insertar(T Entidad);
        bool Actualizar(T Entidad);
        bool Eliminar(ObjectId Id);
    }
}
