﻿using System;

using HelpPoint.GUI.Movil.Models;

namespace HelpPoint.GUI.Movil.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.Text;
            Item = item;
        }
    }
}
