﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.DAL
{
    public class GenericRepository<T> : IManagerRepository<T> where T : BaseDTO
    {

        private MongoClient client;
        private IMongoDatabase db;
     //   private bool idEsAutoNumerico;
        private AbstractValidator<T> _validator;
        ValidationResult resultadoValidador;

        public GenericRepository(AbstractValidator<T> validator)
        {
            
            client = new MongoClient(new MongoUrl("mongodb://helppoint:helppoint123@ds145369.mlab.com:45369/helppoint"));
            db = client.GetDatabase("helppoint");
            _validator = validator;    
        }

        public string Error { get; private set; }

        private IMongoCollection<T> Collection() => db.GetCollection<T>(typeof(T).Name);

        public IEnumerable<T> Leer => Collection().AsQueryable().ToList();

        public bool Actualizar(T Entidad)
        {
            try
            {
                return Collection().ReplaceOne(p => p.Id == Entidad.Id, Entidad).ModifiedCount == 1;
            }
            catch (Exception)
            {
                Error = "No se pudo modificar " + typeof(T).Name;
                return false;
            }
        }

        public bool Eliminar(ObjectId Id)
        {
            try
            {
                return Collection().DeleteOne(p => p.Id == Id).DeletedCount == 1;
            }
            catch (Exception)
            {
                Error = "No se puede eliminar " + typeof(T).Name;
                return false;

            }
        }

        public bool Insertar(T Entidad)
        {
            Entidad.Id = ObjectId.GenerateNewId();
            resultadoValidador = _validator.Validate(Entidad);
            try
            {
                if (resultadoValidador.IsValid)
                {

                    Collection().InsertOne(Entidad);
                    Error = null;
                    return true;
                }
                else
                {
                    Error = typeof(T).Name + " invalida:";
                    foreach (var error in resultadoValidador.Errors)
                    {
                        Error += "\n" + error.ErrorMessage;
                    }

                    return false;
                }
            }
            catch (Exception)
            {
                Error = "No se ha podido crear " + typeof(T).Name;
                return false;
            }

        }
    }
}
