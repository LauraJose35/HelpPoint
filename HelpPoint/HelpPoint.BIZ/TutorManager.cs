﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.BIZ
{
    public class TutorManager : GenericManager<Tutores>, IManagerTutor
    {
        public TutorManager(IManagerRepository<Tutores> repositorio) : base(repositorio)
        {
        }
    }
}
