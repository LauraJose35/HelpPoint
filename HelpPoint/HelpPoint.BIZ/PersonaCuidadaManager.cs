﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.BIZ
{
    public class PersonaCuidadaManager : GenericManager<PersonaCuidada>, IManagerPersonaCuidada
    {
        public PersonaCuidadaManager(IManagerRepository<PersonaCuidada> repositorio) : base(repositorio)
        {
        }
    }
}
