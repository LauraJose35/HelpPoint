﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.BIZ
{
    public class DetalleTutoresManager : GenericManager<DetalleTutores>, IManagerDetalleTutores
    {
        public DetalleTutoresManager(IManagerRepository<DetalleTutores> repositorio) : base(repositorio)
        {
        }
    }
}
