﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Validadores;
using HelpPoint.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.BIZ
{
    public class FabricManager
    {
        public AlertasManager AlertasManager() => new AlertasManager(new GenericRepository<Alertas>(new ValidadorAlertas()));
        public PersonaCuidadaManager PersonaCuidadaManager() => new PersonaCuidadaManager(new GenericRepository<PersonaCuidada>(new ValidadorPersonaCuidada()));
        public TutorManager TutorManager() => new TutorManager(new GenericRepository<Tutores>(new ValidadorTutor()));
        public UbicacionesManager UbicacionesManager() => new UbicacionesManager(new GenericRepository<Ubicaciones>(new ValidadorUbicaciones()));
    }
}
