﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.BIZ
{
    public class AlertasManager : GenericManager<Alertas>, IManagerAlertas
    {
        public AlertasManager(IManagerRepository<Alertas> repositorio) : base(repositorio)
        {
        }
    }
}
