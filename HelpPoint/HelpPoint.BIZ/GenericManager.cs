﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelpPoint.BIZ
{
    public class GenericManager<T> : IManagerGeneric<T> where T : BaseDTO
    {
        internal IManagerRepository<T> repository;
        public GenericManager(IManagerRepository<T> repositorio)
        {
            repository = repositorio;
        }
        public string Error
        {
            get
            {
                return repository.Error;
            }
        }

        public IEnumerable<T> Leer
        {
            get
            {
                return repository.Leer;
            }
        }

        public bool Actualizar(T entidad) => repository.Actualizar(entidad);

        public T BuscarPorId(ObjectId Id) => Leer.Where(p => p.Id == Id).SingleOrDefault();

        public bool Eliminar(ObjectId entidad) => repository.Eliminar(entidad);

        public bool Insertar(T entidad) => repository.Insertar(entidad);


    }

}
