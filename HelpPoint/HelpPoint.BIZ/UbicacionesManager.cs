﻿using HelpPoint.COMMON.Entidades;
using HelpPoint.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelpPoint.BIZ
{
    public class UbicacionesManager : GenericManager<Ubicaciones>, IManagerUbicaciones
    {
        public UbicacionesManager(IManagerRepository<Ubicaciones> repositorio) : base(repositorio)
        {
        }
              
    }
}
